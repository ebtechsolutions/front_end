import { Component } from '@angular/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import { Router } from '@angular/router';
import { fadeAnimation } from './animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [fadeAnimation]
})
export class AppComponent {
  title = 'eb-tech-solutions-app';

  navLinks: any[];
  activeLinkIndex = -1;

  constructor(private router: Router) {
    this.navLinks = [
        {
            label: 'HOME',
            link: './home',
            index: 0
        }, {
            label: 'ABOUT US',
            link: './aboutUs',
            index: 1
        }, {
            label: 'OUR OFFERS',
            link: './ourProducts',
            index: 2
        }, {
          label: 'CONTACT US',
          link: './ourContactDetails',
          index: 3
      },{
        label: 'LOGIN',
        link: './login',
        index: 4
    },{
      label: 'DASHBOARD',
      link: './userDashboard',
      index: 5
  },
    ];
}

ngOnInit(): void {
  this.router.events.subscribe((res) => {
      this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
  });
}
 
}