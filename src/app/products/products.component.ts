import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { ProductsService } from '../services/products/products.service';
import { Cart } from '../models/cart';
import { RespMessage } from '../models/respMessage';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  productForm: FormGroup;
  staticWebForm: FormGroup;  
  cart: Cart = new Cart();
  submitted = false;
  types:any[];
  packageTypes:any[];
  respMsg :RespMessage = new RespMessage();
  public loginUser:string = null;
  public showType:boolean = false;
  public showPkg:boolean = false;
  public showServ:boolean = true;
  public showForm:boolean = false;
  submitButton = 'Submit request';
  isLoading = false;
  pagePrice = 1000;
  total = 1000;  

  constructor(private formBuilder: FormBuilder, private toastrService : ToastrService, 
    private productsService : ProductsService) { }

  ngOnInit(): void {
    this.loginUser = localStorage.getItem('user');

    this.productForm = this.formBuilder.group({
      serviceType: ['', Validators.required],
      packageType: ['', Validators.required],
      packageSubType: ['', Validators.required],

    });
    this.staticWebForm = this.formBuilder.group({
      pages: ['1', ],
      pagesValue: ['R'+this.pagePrice, ],
      total: ['R'+this.total, ],
    });
    
    //Clear all validation variables
   this.reset();

  }
    // convenience getter for easy access to form fields
    get f() { return this.productForm.controls; }
    get f1() { return this.staticWebForm.controls; }

    reset(){
      this.types = null
      this.packageTypes = null;      
      this.showType = false;
      this.showPkg = false;
      this.showServ = true;
      this.showForm = false;
      this.productForm.reset();
      this.staticWebForm.reset();
    }

    onSubmit() {
      this.submitted = true;
  }

  staticSubmit() {
    this.submitted = true;
    //Set missing fields before back end service call
    this.cart.loggedInUser= this.loginUser;
    this.cart.packageStatus = "Pending"
    this.cart.packageCost = this.total.toString();
    this.save();
    this.onLoad("staticWeb");

   // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.staticWebForm.value))
}

  changedService(value) {
console.log(this.loginUser);
    if(this.loginUser != null){
      if(value == "software"){
        this.types = [{ title: "Please select package type", value: "" }, { title: "Website application", value: "web" }, 
        { title: "Mobile application", value: "mobile" }];
        this.showType = true;
      }
  
      if(value == "hardware"){
        this.types = [{ title: "Please select package type", value: "" }, { title: "Need work 1", value: "work1" }, 
        { title: "Need work 2", value: "work2" }];
        this.showType = true;
      }
  
      if(value == "consulting"){
        this.types = [{ title: "Please select package type", value: "" }, { title: "Need work 3", value: "work3" }, 
        { title: "Need work 4", value: "work4" }];
        this.showType = true;
      }
    }else {
      this.toastrService.warning("Oops!! you are not logged in. Please login to continue on this page.");   
    }

  }

  changedType(value) {
    if(this.loginUser != null){
      if(value == "web"){
        this.packageTypes = [{ title: "Please select package format", value: "" }, { title: "Static website", value: "static" },
        { title: "Dynamic website", value: "dynamic" }, { title: "E-Commerce website", value: "eCom" }];
        this.showPkg = true;
      }

      if(value == "mobile"){
        this.packageTypes = [{ title: "Please select package format", value: "" }, { title: "Android application", value: "android" },
        { title: "iOS application", value: "iOS" }, { title: "Both android & iOS application", value: "both" }];
        this.showPkg = true;
      }
  
    //  if(value == "work1"){
     // }
    }else {
      this.toastrService.warning("Oops!! you are not logged in. Please login to continue on this page.");
    }
    
  }

  changedSubType(value) {
    if(this.loginUser != null){
      if(value == "static"){
        this.showForm = true;
      }

      if(value == "dynamic"){
        this.showForm = true;

        //Set default Dynamic web prices
        this.pagePrice = 1500;
        this.total = 1500*2;
        this.staticWebForm = this.formBuilder.group({
          pages: ['2', ],
          pagesValue: ['R'+this.pagePrice, ],
          total: ['R'+this.total, ],
        });
      }
  
      if(value == "eCom"){
        this.showForm = true;
        this.pagePrice = 5000;

        //Set default Dynamic web prices
        this.pagePrice = 5000;
        this.total = 5000*50;
        this.staticWebForm = this.formBuilder.group({
        pages: ['50', ],
        pagesValue: ['R'+this.pagePrice, ],
        total: ['R'+this.total, ],
               });
      }
    }else {
      this.toastrService.warning("Oops!! you are not logged in. Please login to continue on this page.");
    }
    
  }

  onchangeQty(value) {
    console.log(value);
   this.total = this.pagePrice*value;
    this.staticWebForm = this.formBuilder.group({
      pages: [value, ],
      pagesValue: ['R'+this.pagePrice, ],
      total: ['R'+this.total, ],
    });
  }

  onLoad(type){
    //loading for login button
    console.log("START PROCESS");
    if(type == "staticWeb"){
      this.isLoading = true;
      this.submitButton = 'Processing';     
      setTimeout(() => {
        this.isLoading = false;
        this.submitButton = 'Submit request';
      }, 6000)
    }
    //loading for register button
  /*  if(type == "mobile"){
      this.isLoading = true;
      this.submitButton = 'Processing';     
      setTimeout(() => {
        this.isLoading = false;
        this.submitButton = 'Register';
      }, 6000)
    } */
  
  }

  save(): any {
    this.productsService.createCart(this.cart)
      .subscribe(data => {console.log(data), error => console.log(error)
        this.respMsg = data;
        console.log("message = "+this.respMsg.message);
        console.log("status = "+this.respMsg.status);
        if(this.respMsg.status == "success"){  
          this.toastrService.success("Your package was a success. You'll be hearing from us as soon as we can.");
           //Need to reset to login tab if registration is a success
           this.showType = false;
           this.showPkg = false;
           this.showServ = true;
           this.showForm = false;
        }else{
          this.toastrService.error("Sorry, an error occerred why sending your request. Please try again.");
         } 
      });      
  }


}
