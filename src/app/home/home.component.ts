import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
  ],
})
export class HomeComponent implements OnInit {

  public loginUser:String = null;
 
  event_list = [
    {
      event:' Event 1',
      eventType:'WEB DEVELOPMENT SERVICES',
      eventDescription:'Epirus Bow Tech Solutions provides best Customer focused web development services built on the latest technologies.'+ 
      'We expertise in Docker, Angular, PHP, Spring boot, SQL and NO SQL website development.',
      img: 'assets/images/app-images/web-development.jpg',
    },
     {
      event:' Event 2',
      eventType:'MOBILE DEVELOPMENT SERVICES',
      eventDescription:'Epirus Bow, is a Mobile app Development Company will sculpture many ingenious solutions right from the inception to launch.'+ 
      'Our custom mobile apps development services are strictly based on our client’s requirements and end up providing solutions that are second to none.'+ 
      'Our mobile app developers have phenomenal experience in designing the UI / UX of Mobile Apps and are also proficient in iOS and Android development.',
      img: 'assets/images/app-images/mobile-app-development.jpg',
    },
     {
      event:' Event 3',
      eventType:'HARDWARE SERVICES',
      eventDescription:'Here at Epirus Bow, not only do we create applications but we also maintain and upgrade the hardware they run on.'+
      'We do Network connection instalation and upgrades and Software upgrades eg. Anti-virus instalations and upgrades.',
      img: 'assets/images/app-images/hardware-service.jpg',
    },
     {
      event:' Event 4',
      eventType:'CONSULTING SERVICES',
      eventDescription:'Epirus Bow also offers consultation to small and large businesses looking to reduce the workload(Outsourcing).'+
      'We also offer consultation or advice on web and mobile developments.',
      img: 'assets/images/app-images/consulting-service.jpg',
    },

    {
      event:' Event 5',
      eventType:'STATIC WEBSITE PRICES',
      eventDescription:'TThese are the starting prices of static websites here at Epirus Bow.',
      img: 'assets/images/app-images/ebTechPrices.JPG',
    },

    {
      event:' Event 6',
      eventType:'DYNAMIC WEBSITE PRICES',
      eventDescription:'These are the starting prices of dynamic websites here at Epirus Bow.',
      img: 'assets/images/app-images/ebTechPricesDynamic.JPG',
    },

    {
      event:' Event 7',
      eventType:'E-Commerce WEBSITE PRICE',
      eventDescription:'This is the starting price of a full E-Commerce website here at Epirus Bow.',
      img: 'assets/images/app-images/eComPrice.JPG',
    },
  ]

  past_events = this.event_list;

  constructor(private router: Router, private toastrService : ToastrService) {
  }

  ngOnInit() {
    this.loginUser = localStorage.getItem('user');
    console.log(this.loginUser);
  }

  
  goToPackages(){
    this.router.navigate(['/ourProducts']);
  }

}
