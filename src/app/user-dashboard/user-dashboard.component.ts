import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import { ToastrService } from 'ngx-toastr';

import { CartService } from '../services/cart/cart.service';
import { Cart } from '../models/cart';
import { RespMessage } from '../models/respMessage';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {


  carts: Cart[] = [];
  public loginUser:String = null;
  public show:boolean = false;
  selectedOpt: string;
  options:any[];
  selection = new SelectionModel<Cart>(true, []);
  pageTitle = localStorage.getItem('user');
  imageWidth = 45;
  imageMargin = 1;
  showImage = true;
  _listFilter = '';
  get listFilter(): string {
      return this._listFilter;
  }

  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredGames = this.listFilter ? this.doFilter(this.listFilter) : this.games;
}

filteredGames: any[] = [];
games: Cart[] = [];

  constructor(private router: Router, private toastrService : ToastrService,
    private cartService : CartService) {       
      
      this.carts = this.getAllCarts(); 
      this.listFilter = '';
    }

    doFilter(filterBy: string): Cart[] {
      filterBy = filterBy.toLocaleLowerCase();
      return this.games.filter((game: Cart) =>
          game.loggedInUser.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }

  toggleImage(): void {
      this.showImage = !this.showImage;
  }

    

  ngOnInit(): void {
    this.loginUser = localStorage.getItem('user');
    if(this.loginUser != null){
        this.show = true;
    }else{
      this.toastrService.warning("Oops!! you are not logged in. Please login to access this page.");
      this.router.navigate(['/login']);
    }
    //localStorage.removeItem('user');
    //console.log(localStorage.getItem('user'));

    //Get all product in cart for the user
    this.getAllCarts();
    this.options = [
      {value: 'addService', viewValue: 'Add more services'},
      {value: 'home', viewValue: 'Go to home page'},
      {value: 'sign-out', viewValue: 'sign-out'}
    ];
    
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.carts.forEach(row => this.selection.select(row));
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.carts.length;
    return numSelected === numRows;
  }

  checkboxLabel(row?: Cart): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }
  //Get data from spring backend server
  getAllCarts():any{
    this.cartService.getCartList()
    .subscribe(data => {console.log(data), error => console.log(error)
      this.carts = data;
      console.log("<<< Cart data >>>>");
      console.log(this.carts);
    });
  }
  
  onChanged(value){
    console.log("Test onChanged>>>")
    console.log(value);
    if(value == "addService"){
      this.router.navigate(['/ourProducts']);
    }

    if(value == "home"){
      this.router.navigate(['/home']);
    }

    if(value == "sign-out"){
      localStorage.removeItem('user');
      this.toastrService.info("Thank you for using our services. Please do come back soon.");
      this.router.navigate(['/login']);
    }
  }

}
