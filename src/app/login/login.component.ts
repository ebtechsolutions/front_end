import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { PersonService } from '../services/person/person.service';
import { Person } from '../models/person';
import { RespMessage } from '../models/respMessage';

// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../_helpers/must-match.validator';
//import { ValidateID } from '../_helpers/identity-check.validator';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  registerForm: FormGroup;
  public show:boolean = false;
  public idShow:boolean = true;
  public passShow:boolean = false;
  submitted = false;
  submittedLogin = false;
  user : Person = new Person();
  persons: Person[];
  respMsg :RespMessage = new RespMessage();
  map = new Map<String, String>();
  LoginButton = 'Login';
  regButton = 'Register';
  isLoading = false;
  

    constructor(private formBuilder: FormBuilder, private personService: PersonService, 
      private router: Router, private toastrService : ToastrService) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            idNo:  ['', [Validators.required, Validators.maxLength(13)]],
            passPortNo: ['0000', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(8)]],
            username: ['', Validators.required],
            confirmPassword: ['', Validators.required]
        }, {
            validator: MustMatch('password', 'confirmPassword', 'idNo')
        });

        this.loginForm = this.formBuilder.group({
          username: ['', Validators.required],
          password: ['', Validators.required]
      });

    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }
    get f1() { return this.loginForm.controls; }

    checkId(value){
        console.log("<<< Id value >>>>");
        console.log(value);
        let strngValue = new String(value);
        if(strngValue.length == 4){
          let mnth = strngValue.slice(-2);
            if(Number(mnth) > 12){
              console.log("is not valid =  "+mnth)
            }
         
        }
        
        if(strngValue.length == 6){
          let mnth = strngValue.slice(-2);
            if(Number(mnth) > Number("02")){
              
              console.log("is  invalid day =  "+mnth)
            }
         
        }
             
    }

    onSubmit() {
        this.submitted = true;

        // stop here if register form is invalid
        if (this.registerForm.invalid) {
            return;
        }     
       //Call spring save service for user register
       this.save();
       this.onLoad("register"); 
    }

    onLogin() {
      this.submittedLogin = true;

            // stop here if login form is invalid
            if (this.loginForm.invalid) {
              return;
          }
  
          //Call spring save service for user login
          this.loginUser();       
         this.onLoad("login");
    }

  onchange(event: Event) {
    this.show = true;
    this.registerForm.reset();
      this.loginForm.reset();
      this.submitted = false;
      this.submittedLogin = false;
  }

  onchangeReg(event: Event) {
    this.show = false;
    this.registerForm.reset();
      this.loginForm.reset();
      this.submitted = false;
      this.submittedLogin = false;
  }

  onchangeId(value) {
    console.log(value._checked);
    if(!value._checked){
      this.passShow = true;
      this.idShow = false;
      this.registerForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        idNo:  ['9912310000085', [Validators.required, Validators.maxLength(13)]],
        passPortNo: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(8)]],
        username: ['', Validators.required],
        confirmPassword: ['', Validators.required]
    }, {
        validator: MustMatch('password', 'confirmPassword', 'idNo')
    });
    }
    if(value._checked){
      this.passShow = false;
    this.idShow = true;
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      idNo:  ['', [Validators.required, Validators.maxLength(13)]],
      passPortNo: ['000000', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      username: ['', Validators.required],
      confirmPassword: ['', Validators.required]
  }, {
      validator: MustMatch('password', 'confirmPassword', 'idNo')
  });
    }
    
    
  }


//Addition of spring back_end service call for login and registration
save(): any {
  this.personService.createPerson(this.user)
    .subscribe(data => {console.log(data), error => console.log(error)
      this.respMsg = data;
      if(this.respMsg.status == "success"){  
        this.toastrService.success("Your registration was a success. Welcome to the Epirus Bow family.");
        this.registerForm.reset();
        this.show = true;
        this.submitted = false;
        this.submittedLogin = false;
      }else{
        this.toastrService.error("Sorry, provided username is already in use. Please enter another username");
                //Need to reset to login tab if registration is a success
                this.show = true;
                this.submitted = false;
                this.submittedLogin = false;
       } 
    });      
}

loginUser(): any {  
   this.personService.LoginUser(this.user)
      .subscribe( data => {console.log(data), error => console.log(error)      
       this.respMsg = data;
       if(this.respMsg.status == "success"){
        let loginUser = this.user.username;
        localStorage.setItem('user', loginUser);
        this.toastrService.success("Welcome back again, "+localStorage.getItem('user'));
        this.router.navigate(['/userDashboard']);
       }else{
        this.toastrService.error("Oops!! Login failed. Please check your credentials and try again.");
       }   
      });
};

onLoad(type){
  //loading for login button
  if(type == "login"){
    this.isLoading = true;
    this.LoginButton = 'Processing';     
    setTimeout(() => {
      this.isLoading = false;
      this.LoginButton = 'Login';
    }, 6000)
  }
  //loading for register button
  if(type == "register"){
    this.isLoading = true;
    this.LoginButton = 'Processing';     
    setTimeout(() => {
      this.isLoading = false;
      this.regButton = 'Register';
    }, 6000)
  } 

}

}
