import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  public showLu:boolean = false;
  public showLu2:boolean = false;
  public showMk:boolean = false;
  public showMk2:boolean = false;
  aboutUsForm: FormGroup;;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.aboutUsForm = this.formBuilder.group({
  });
  }

  onchangeLuMore(event: Event) {
    this.showLu = true;
    this.showLu2 =true;
  }

  onchangeLuLess(event: Event) {
    this.showLu = false;
    this.showLu2 =false;
  }

  onchangeMkMore(event: Event) {
    this.showMk = true;
    this.showMk2 =true;
  }

  onchangeMkLess(event: Event) {
    this.showMk = false;
    this.showMk2 =false;
  }

}
