export class Cart {
    id: number;
    loggedInUser: string;
    serviceType: string;
    packageType: string;
    packageFormat: string;
    pageQuantity: string;
    packageCost: string;
    packageStatus: string;
    packageDscrptn: string;
}