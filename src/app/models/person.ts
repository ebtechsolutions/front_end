export class Person {
    id: number;
    name: string;
    surname: string;
    idNo: string;
    passPortNo: string;
    email: string;
    username: string;
    password: string;
}