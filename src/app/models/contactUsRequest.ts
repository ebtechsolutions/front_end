export class ContactUsRequest {
    id: number;
    name: string;
    email: string;
    subject: string;
    message: string;
    status: string;    
    response: string;
}