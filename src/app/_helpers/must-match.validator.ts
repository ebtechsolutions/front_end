import { FormGroup } from '@angular/forms';

// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string, controlNameID: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];
        const controlID = formGroup.controls[controlNameID];

        // RSA ID validation string
        let ex = /^(((\d{2}((0[13578]|1[02])(0[1-9]|[12]\d|3[01])|(0[13456789]|1[012])(0[1-9]|[12]\d|30)|02(0[1-9]|1\d|2[0-8])))|([02468][048]|[13579][26])0229))(( |-)(\d{4})( |-)(\d{3})|(\d{7}))/;
      
        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }

        let theIDnumber = controlID.value;
        let strngValue = new String(controlID.value);;
       if(strngValue.length != 13){
           // alert code goes here
          controlID.setErrors({ maxlength: true });
          return false; 
        }
        if (ex.test(theIDnumber) == false) {
          // alert code goes here
          controlID.setErrors({ idValid: true });
          return false;
        } else {
            controlID.setErrors(null);
        }
        return false;
        
    }
}