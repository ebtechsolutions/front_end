import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { ContactUsService } from '../services/contactUs/contact-us.service';
import { ContactUsRequest } from '../models/contactUsRequest';
import { RespMessage } from '../models/respMessage';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {

  contactForm: FormGroup;
  contactUsRequest : ContactUsRequest = new ContactUsRequest();
  respMsg :RespMessage = new RespMessage();
  submitted = false;
  disabledSubmitButton = false;
  isLoading = false;
  submitButton = 'Send request';

  constructor(private formBuilder: FormBuilder, private contactUsService: ContactUsService, 
    private router: Router, private toastrService : ToastrService) { }

  ngOnInit(): void {
    this.contactForm = this.formBuilder.group({
      contactFormName: ['', Validators.required],
      contactFormEmail: ['', [Validators.required, Validators.email]],
      contactFormSubjects: ['', Validators.required],
      contactFormMessage: ['', Validators.required]
  });
  }

  // convenience getter for easy access to form fields
  get f() { return this.contactForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if about us form is invalid
    if (this.contactForm.invalid) {
        return;
    }

    //Need to set missing status and response values
    this.contactUsRequest.status = "Pending request";
    this.contactUsRequest.response = "You currently have no response on your request";
    this.save();
    this.onLoad();

   // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.contactForm.value))
}

save(): any {
  this.contactUsService.createContactUs(this.contactUsRequest)
    .subscribe(data => {console.log(data), error => console.log(error)
      this.respMsg = data;
      if(this.respMsg.status == "success"){  
        this.toastrService.success("Your request was processed successfully. Our team will get back to you as soon as possible.");
        this.contactForm.reset();
        this.submitted = false;
      }else{
        this.toastrService.error("Sorry, your request was unsuccessful. Please try to send it again");
        this.submitted = false;
       } 
    });      
}

onLoad(){
  //loading for login button
  
    this.isLoading = true;
    this.submitButton = 'Processing';     
    setTimeout(() => {
      this.isLoading = false;
      this.submitButton = 'Send Request';
    }, 6000)
  }

}
