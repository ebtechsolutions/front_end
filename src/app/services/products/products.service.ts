import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private baseUrl = 'http://localhost:9090/api/carts';
  map = new Map<String, String>();

  constructor(private http: HttpClient) { }

  getCartList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  createCart(cart: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/`+ `create`, cart);
  }
}
