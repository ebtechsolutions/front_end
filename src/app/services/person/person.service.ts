import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PersonService {

  private baseUrl = 'http://localhost:9090/api/persons';
  map = new Map<String, String>();
   

  constructor(private http: HttpClient) { }

  getPersonsList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  createPerson(person: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/`+ `create`, person);
  }

  getPersonByUsername(username: String): Observable<any> {
    return this.http.get(`${this.baseUrl}/username/${username}`);
  }

  public LoginUser(user): any{
    return this.http.post(`${this.baseUrl}` + `/`+ `login`, user);
  }
}
