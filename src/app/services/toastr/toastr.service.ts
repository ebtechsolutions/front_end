import { Injectable } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
declare var toastr:any

@Injectable({
  providedIn: 'root'
})
export class ToastrService {

  constructor(private toastrService : ToastrModule) { }

  Success(title: string,meassage?:string){
    toastr.success(title,meassage)
  }

  Warning(title: string,meassage?:string){
    toastr.warning(title,meassage)
  }

  Error(title: string,meassage?:string){
    toastr.error(title,meassage)
  }

  Info(title: string,meassage?:string){
    toastr.info(title,meassage)
  }
}
