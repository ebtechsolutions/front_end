import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactUsService {

  private baseUrl = 'http://localhost:9090/api/contactUs';
  map = new Map<String, String>();

  constructor(private http: HttpClient) { }

  getContactUsRequestList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  createContactUs(contactUsRequest: Object): Observable<any> {
    return this.http.post(`${this.baseUrl}` + `/`+ `create`, contactUsRequest);
  }

  getContactUsByStatus(status: String): Observable<any> {
    return this.http.get(`${this.baseUrl}/findByStatus/${status}`);
  }

}
